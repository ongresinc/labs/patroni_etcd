
## Quorum Lost & Cluster Misconfiguration

[Quorum Lost](https://etcd.io/docs/v3.5/learning/design-learner/#24-quorum-lost)
[Cluster Misconfiguration](https://etcd.io/docs/v3.5/learning/design-learner/#3-cluster-misconfigurations)

As seen above, a simple misconfiguration can fail the whole cluster into an inoperative state. In such case, an operator need manually recreate the cluster with `etcd --force-new-cluster` flag.

## Cluster Split 2 + 2

> See [Cluster Split 2 + 2](https://etcd.io/docs/v3.5/learning/design-learner/#23-cluster-split-22)

## Learners (etcd v3.4+)

`member add --learner`
`member promote`

Learners do not vote, but can be promoted to voters.

Proposed features for future releases:

Make learner state only and default: Defaulting a new member state to learner will greatly improve membership reconfiguration safety, because learner does not change the size of quorum. Misconfiguration will always be reversible without losing the quorum.

**Make voting-member promotion fully automatic**: Once a learner catches up to leader’s logs, a cluster can automatically promote the learner. etcd requires certain thresholds to be defined by the user, and once the requirements are satisfied, learner promotes itself to a voting member. From a user’s perspective, “member add” command would work the same way as today but with greater safety provided by learner feature.

**Make learner standby failover node**: A learner joins as a standby node, and gets automatically promoted when the cluster availability is affected.

Make learner read-only: A learner can serve as a read-only node that never gets promoted. In a weak consistency mode, learner only receives data from leader and never process writes. Serving reads locally without consensus overhead would greatly decrease the workloads to leader but may serve stale data. In a strong consistency mode, learner requests read index from leader to serve latest data, but still rejects writes.

## Mirror Maker and Learners

[Quote](https://etcd.io/docs/v3.5/learning/design-learner/#learner-vs-mirror-maker):

> etcd implements “mirror maker” using watch API to continuously relay key creates and updates to a separate cluster. **Mirroring usually has low latency overhead once it completes initial synchronization**. Learner and mirroring overlap in that both can be used to replicate existing data for read-only. 

> However, mirroring does not guarantee `linearizability`. 

Which, in the case of Customer XXX, is not a problem, as Patroni should start voting too after etcd cluster is back.
We don't need most up to date keys, as there are not too many options in the DC2 (only 1 node, no preferred replica).
Also, `synchronous_commits` are enabled, which should be an additional layer of durability.

But, **we need quorum and the quorum between mirrored clusters and the source are not the same**.

> During network disconnects, previous key-values might have been discarded, and clients are expected to verify watch responses for correct ordering. Thus, there is no ordering guarantee in mirror. Use mirror for minimum latency (e.g. cross data center) at the costs of consistency. Use learner to retain all historical data and its ordering.
