
## [etcd-trigger](https://github.com/sheldonh/etcd-trigger/blob/master/etcd-trigger.go)

It acts as a http request proxy between etcd and any other service hearing key changes.
Creates a lease, uses the etcd client lib.

## [confd on isolated container](https://info.tail-f.com/hubfs/Whitepapers/AppNote_ConfD-on%20Docker%20Final.pdf)

[confd](https://github.com/kelseyhightower/confd) stale releases, last 0.16.0 on 2078 but stable.


## [confd docker example](https://github.com/m-richo/docker-confd-example)




## [confd inside container](https://github.com/dockage/confd)

Only works for interacting with hosts, not other containers.

## [etcd-watcher](https://github.com/casbin/etcd-watcher)

Only for example, works only for casbin.

## Insights

[Kukushkin regarding easiest solutions for patroni/etcd](https://github.com/zalando/patroni/issues/861#issuecomment-438174169)

