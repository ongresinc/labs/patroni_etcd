

[Learner feature](https://github.com/etcd-io/etcd/issues/10537)


; -v /usr/share/ca-certificates/:/etc/ssl/certs

docker run \
 --ip 172.16.100.200 -p 4001:4001 -p 2380:2380 -p 2379:2379  --network=patroni_etcd_patroni-etcd \
 --name etcd4 quay.io/coreos/etcd:v3.4.15  \
 /usr/local/bin/etcd -name etcd4 \
 -advertise-client-urls http://172.16.100.200:2379,http://etcd4:4001 \
 -listen-client-urls http://0.0.0.0:2379,http://0.0.0.0:4001 \
 -initial-advertise-peer-urls http://172.16.100.200:2379 \
 -listen-peer-urls http://0.0.0.0:2380 \
 -initial-cluster-token mys3cr3ttok3n \
 -initial-cluster etcd1=http://etcd1:2380 \
 -initial-cluster-state=existing


