
## etcd intro

[etcd online sandbox](http://play.etcd.io/play)
[code for play.etcd.io](https://github.com/etcd-io/etcdlabs)

[libs available in etcd](https://go.etcd.io/)

### API

https://etcd.io/docs/v3.2/learning/api/
